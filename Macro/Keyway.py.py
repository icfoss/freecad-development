#Select a cylindrical part and apply a keyway. make sure the part is laid out with the cylindrical surface facing the user

import FreeCAD
import Sketcher
import PartDesign
import Part

keyMap = {

'6':(2,2),
'7':(2,2),
'8':(2,2),
'9':(3,3),
'10':(3,3),
'11':(4,4),
'12':(4,4),
'13':(5,5),
'14':(5,5),
'15':(5,5),
'16':(5,5),
'17':(5,5),
'18':(6,6),
'19':(6,6),
'20':(6,6),
'21':(6,6),
'22':(6,6),
}

def getKeySize(shaftDia):
	val = keyMap[str(shaftDia)]
	keysize = [val[0],val[1]]
	return keysize

def getShaftDia():

	selection = Gui.Selection.getSelectionEx()[0]
	rad = selection.Object.Radius
	return int(rad*2)
5

def makeKeyWay():
    Gui.activateWorkbench("PartDesignWorkbench")
    shapesel = Gui.Selection.getSelectionEx()[0]
    shapelabel = shapesel.Object.Label
    subElementName = Gui.Selection.getSelectionEx()[0].SubElementNames[0]
    d = getShaftDia()	
    
    App.activeDocument().addObject("Sketcher::SketchObject","Sketch")	
    App.activeDocument().Sketch.Support = (App.ActiveDocument.Cylinder,[subElementName])
    App.activeDocument().recompute()
    Gui.activeDocument().setEdit('Sketch')
    
    rad = d/2
    k = getKeySize(d)
    print k    
    kw = int(k[0]/2)
    kh = int(k[1]/2)
    print kh
    geoList = []
    geoList.append(Part.Line(App.Vector(-kw,rad+kh,0),App.Vector(kw,rad+kh,0)))
    geoList.append(Part.Line(App.Vector(kw,rad+kh,0),App.Vector(kw,rad-kh,0)))
    geoList.append(Part.Line(App.Vector(kw,rad-kh,0),App.Vector(-kw,rad-kh,0)))
    geoList.append(Part.Line(App.Vector(-kw,rad-kh,0),App.Vector(-kw,rad+kh,0)))
    App.activeDocument().Sketch.addGeometry(geoList,False)
    App.activeDocument().recompute()
    App.activeDocument().addObject("PartDesign::Pad","Pad")
    App.activeDocument().Pad.Sketch = App.activeDocument().Sketch
    App.activeDocument().Pad.Length = 10.0
    App.activeDocument().Pad.Reversed = 1
    App.activeDocument().recompute()
# activate Part workbench
#    Gui.activateWorkbench("PartWorkbench")
#    App.activeDocument().addObject("Part::Cut","Cut")
#    App.activeDocument().Cut.Base = App.activeDocument().Cylinder
#    App.activeDocument().Cut.Tool = App.activeDocument().Pad
#    App.ActiveDocument.recompute()
# activate PartDesign workbench
makeKeyWay()

