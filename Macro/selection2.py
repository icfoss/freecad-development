# -*- coding: utf-8 -*-

# Macro Begin: C:\Users\SAMSUNG\Desktop\selection2.FCMacro +++++++++++++++++++++++++++++++++++++++++++++++++
import FreeCAD
import Sketcher
import PartDesign
import Part

#Gui.activeDocument().activeView().viewFront()
#Gui.activeDocument().activeView().viewRight()
#Gui.activeDocument().activeView().viewTop()
App.activeDocument().addObject('Sketcher::SketchObject','Sketch')
App.activeDocument().Sketch.Placement = App.Placement(App.Vector(0.000000,0.000000,0.000000),App.Rotation(0.000000,0.000000,0.000000,1.000000))
#Gui.activeDocument().activeView().setCamera('#Inventor V2.1 ascii \n OrthographicCamera {\n viewportMapping ADJUST_CAMERA \n position 0 0 87 \n orientation 0 0 1  0 \n nearDistance -112.88701 \n farDistance 287.28702 \n aspectRatio 1 \n focalDistance 87 \n height 143.52005 }')
#Gui.activeDocument().setEdit('Sketch')
geoList = []
geoList.append(Part.Line(App.Vector(-2.727580,11.135271,0),App.Vector(3.047624,11.135271,0)))
geoList.append(Part.Line(App.Vector(3.047624,11.135271,0),App.Vector(3.047624,7.951507,0)))
geoList.append(Part.Line(App.Vector(3.047624,7.951507,0),App.Vector(-2.727580,7.951507,0)))
geoList.append(Part.Line(App.Vector(-2.727580,7.951507,0),App.Vector(-2.727580,11.135271,0)))
App.ActiveDocument.Sketch.addGeometry(geoList,False)
conList = []
conList.append(Sketcher.Constraint('Coincident',0,2,1,1))
conList.append(Sketcher.Constraint('Coincident',1,2,2,1))
conList.append(Sketcher.Constraint('Coincident',2,2,3,1))
conList.append(Sketcher.Constraint('Coincident',3,2,0,1))
conList.append(Sketcher.Constraint('Horizontal',0))
conList.append(Sketcher.Constraint('Horizontal',2))
conList.append(Sketcher.Constraint('Vertical',1))
conList.append(Sketcher.Constraint('Vertical',3))
App.ActiveDocument.Sketch.addConstraint(conList)

#Gui.activeDocument().resetEdit()
App.ActiveDocument.recompute()
App.activeDocument().addObject("PartDesign::Pad","Pad")
App.activeDocument().Pad.Sketch = App.activeDocument().Sketch
App.activeDocument().Pad.Length = 10.0
App.ActiveDocument.recompute()
#Gui.activeDocument().hide("Sketch")
#Gui.activeDocument().setEdit('Pad',0)
App.ActiveDocument.Pad.Length = 10.000000
App.ActiveDocument.Pad.Reversed = 0
App.ActiveDocument.Pad.Midplane = 0
App.ActiveDocument.Pad.Length2 = 100.000000
App.ActiveDocument.Pad.Type = 0
App.ActiveDocument.Pad.UpToFace = None
App.ActiveDocument.recompute()
#Gui.activeDocument().resetEdit()
#Gui.activateWorkbench("PartWorkbench")
App.activeDocument().addObject("Part::Fuse","Fusion")
App.activeDocument().Fusion.Base = App.activeDocument().Shape
App.activeDocument().Fusion.Tool = App.activeDocument().Pad
#Gui.activeDocument().hide("Shape")
#Gui.activeDocument().hide("Pad")
#Gui.ActiveDocument.Fusion.ShapeColor=Gui.ActiveDocument.Shape.ShapeColor
#Gui.ActiveDocument.Fusion.DisplayMode=Gui.ActiveDocument.Shape.DisplayMode
App.getDocument("Unnamed").removeObject("Fusion")
App.activeDocument().addObject("Part::Cut","Cut")
App.activeDocument().Cut.Base = App.activeDocument().Shape
App.activeDocument().Cut.Tool = App.activeDocument().Pad
#Gui.activeDocument().hide("Shape")
#Gui.activeDocument().hide("Pad")
#Gui.ActiveDocument.Cut.ShapeColor=Gui.ActiveDocument.Shape.ShapeColor
#Gui.ActiveDocument.Cut.DisplayMode=Gui.ActiveDocument.Shape.DisplayMode
# Macro End: C:\Users\SAMSUNG\Desktop\selection2.FCMacro +++++++++++++++++++++++++++++++++++++++++++++++++
