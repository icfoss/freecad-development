![LOGO](FreeCAD_logo.png)

Have you bought anything recently? A smartphone or a bike or a shoe or a waste basket?
Have you ever thought about the massive design challenges that have to overcome everyday,
 by engineers, to design a product that you are using everyday?

Govt of Kerala’s ICFOSS and iNCAETEK collaboratively developed macros in FreeCAD for engineering aspirants of mechanical streams to do Product Development 
and Engineering Design using FreeCAD, a free and open-source parametric 3D CAD modeler. 

Macros are a convenient way to reproduce complex actions in FreeCAD. 
You simply record actions as you do them, then save those actions under a name,
and replay them whenever you want.
Since macros are in reality a list of Python commands, you can also edit them,
 and create very complex scripts.

While Python scripts normally have the .py extension, FreeCAD macros should have the .FCMacro extension.

The developed macros tested on  **FreeCAD version : FreeCAD 0.18 ,Python 3.6.6** environment and  includes/provides features like

-  Sheet Metal Costing
-  Design Rule Check(DRC)
    - Sheet Metal DRC
        -  Minimum Hole Diameter
        -  Hole Distance
 		-  Bend Radius
 		-  Hole to Edge Distance
    - Injection Moulding DRC
        -  Wall Thickness
        -  Rib to Wall Thickness
- Plastic Moulding
    - 	Lip and Groove
- Fillet Radius etc..




Usage & Getting help
--------------------

The FreeCAD wiki contains documentation on 
general FreeCAD usage, Python scripting, and development. These 
pages might help you get started:

- [Getting started](http://www.freecadweb.org/wiki/Getting_started)
- [Features list](http://www.freecadweb.org/wiki/Feature_list)
- [Frequent questions](http://www.freecadweb.org/wiki/FAQ)
- [Workbenches](http://www.freecadweb.org/wiki/Workbench_Concept)
- [Scripting](http://www.freecadweb.org/wiki/Power_users_hub)
- [Development](http://www.freecadweb.org/wiki/Developer_hub)

The [FreeCAD forum](http://forum.freecadweb.org) is also a great place
to find help and solve specific problems you might encounter when
learning to use FreeCAD.

